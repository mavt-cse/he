typedef struct He He;
real he_area(He *he, const real *x, const real *y, const real *z);
int he_area_ver(He *he, const real *x, const real *y, const real *z, /**/ real *area);
int he_area_tri(He *he, const real *x, const real *y, const real *z, /**/ real *area);
