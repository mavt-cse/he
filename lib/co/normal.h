/* "mean weighted by angle" */
int normal_mwa(He *he, const real *x, const real *y, const real *z,
                  real *nx, real *ny, real *nz);
