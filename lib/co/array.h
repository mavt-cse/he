real array_min(int, const real[]);
real array_max(int, const real[]);

real array_zero(int, real[]);
int array_zero3(int, real[], real[], real[]);

int array_axpy(int, real, const real[], /**/ real[]);
int array_axpy3(int, real, const real[], const real[], const real[], /**/ real[], real[], real[]);
