typedef struct He He;

int he_equiangulate(He*, const real *x, const real *y, const real *z, /**/ int*);
int he_ear(He*, int e0);
