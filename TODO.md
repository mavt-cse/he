# name change

- Change scripts

    co. -> co.
	
- HE_LDFLAGS, HE_CFLAGS

- I make files
  `P = he -> `P = co`

- directory name in 

`lib/he -> lib/co`

- Library names
`he_d / co_d`, `he_s / co_s`, `he_l / co_l`

- #include "co/" -> #include "co/"

- load("he/.") -> load("he/.")$

- In m4
  include(`co.m4')

+ In some function names he_ / co_
+ he_f_ / co_f_
